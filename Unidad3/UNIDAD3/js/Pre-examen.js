//Diseñe un script que codifica donde se seleccione de tabla de multiplicar 1-10 y me muestre la tabla usando imagenes como se muestra a continuacion 
document.getElementById('mostrarTabla').addEventListener('click', function() {
    var tablaVal = document.getElementById('Tabla').value;
    var tablaMultiplicar = document.getElementById('imprimeTabla');

    tablaMultiplicar.innerHTML = '';

    if(tablaVal==1){
        for(var i = 0; i < 10; i++){
            var imagentablaVal = new Image();
            imagentablaVal.src = "ruta_de_la_imagen" + tablaVal + ".png";

            var signoMulti = new Image();
            signoMulti.src = "/img/x.png";

            var imagenMultiplicador = new Image();
            imagenMultiplicador.src = "/img/" + i + ".png";

            var imagenIgual = new Image();
            imagenIgual.src = "/img/=.png";

            var imagenResultado1 = new Image();
            imagenResultado1.src = "/img/1.png";

            tablaMultiplicar.appendChild(imagentablaVal);
            tablaMultiplicar.appendChild(signoMulti);
            tablaMultiplicar.appendChild(imagenMultiplicador);
            tablaMultiplicar.appendChild(imagenResultado1);


        }
    }

});